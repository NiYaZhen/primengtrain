import { Injectable } from '@angular/core';
import { card } from './webcapage';

@Injectable({
  providedIn: 'root',
})
export class WebcapageService {
  cards: card[] = [];
  getcards(): card[] {
    this.cards = [
      {
        title: `主訴(S)`,
        date: `(2023/4/11)`,
        content: `Fusce volutpat lectus et nisl consectetur finibus. In vitae scelerisque augue, in varius eros. Nunc sapien diam, euismod et pretium id, volutpat et tortor. In vulputate lorem quis dui vestibulum`,
      },
      {
        title: `理學檢查(O)`,
        date: `(2023/4/11)`,
        content: `Vestibulum tempus imperdiet sem ac porttitor. Vivamus pulvinar commodo orci, suscipit porttitor velit elementum non. Fusce nec pellentesque erat, id lobortis nunc. Donec dui leo, ultrices quis turpis nec, sollicitudin sodales tortor. Aenean dapibus `,
      },
      {
        title: `診斷(A)`,
        date: ``,
        content: ``,
      },
      {
        title: `治療計畫(P)`,
        date: `(2023/4/11)`,
        content: `Fusce volutpat lectus et nisl consectetur finibus. In vitae scelerisque augue, in varius eros. Nunc sapien diam, euismod et pretium id, volutpat et tortor. In vulputate lorem quis dui vestibulum, vitae imperdiet diam bibendum. Maecenas scelerisque orci a dolor vestibulum sagittis. Etiam quis finibus arcu, vel efficitur diam. Curabitur felis eros, `,
      },
      {
        title: `治療計畫(P)`,
        date: `(2023/4/11)`,
        content: `Fusce volutpat lectus et nisl consectetur finibus. In vitae scelerisque augue, in varius eros. Nunc sapien diam, euismod et pretium id, volutpat et tortor. In vulputate lorem quis dui vestibulum, vitae imperdiet diam bibendum. Maecenas scelerisque orci a dolor vestibulum sagittis. Etiam quis finibus arcu, vel efficitur diam. Curabitur felis eros, `,
      },
      {
        title: `治療計畫(P)`,
        date: `(2023/4/11)`,
        content: `Fusce volutpat lectus et nisl consectetur finibus. In vitae scelerisque augue, in varius eros. Nunc sapien diam, euismod et pretium id, volutpat et tortor. In vulputate lorem quis dui vestibulum, vitae imperdiet diam bibendum. Maecenas scelerisque orci a dolor vestibulum sagittis. Etiam quis finibus arcu, vel efficitur diam. Curabitur felis eros, `,
      },
      {
        title: `治療計畫(P)`,
        date: `(2023/4/11)`,
        content: `Fusce volutpat lectus et nisl consectetur finibus. In vitae scelerisque augue, in varius eros. Nunc sapien diam, euismod et pretium id, volutpat et tortor. In vulputate lorem quis dui vestibulum, vitae imperdiet diam bibendum. Maecenas scelerisque orci a dolor vestibulum sagittis. Etiam quis finibus arcu, vel efficitur diam. Curabitur felis eros, `,
      },
      {
        title: `治療計畫(P)`,
        date: `(2023/4/11)`,
        content: `Fusce volutpat lectus et nisl consectetur finibus. In vitae scelerisque augue, in varius eros. Nunc sapien diam, euismod et pretium id, volutpat et tortor. In vulputate lorem quis dui vestibulum, vitae imperdiet diam bibendum. Maecenas scelerisque orci a dolor vestibulum sagittis. Etiam quis finibus arcu, vel efficitur diam. Curabitur felis eros, `,
      },
    ];
    return this.cards;
  }
}
