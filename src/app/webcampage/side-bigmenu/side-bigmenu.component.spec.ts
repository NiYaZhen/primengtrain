import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SideBigmenuComponent } from './side-bigmenu.component';

describe('SideBigmenuComponent', () => {
  let component: SideBigmenuComponent;
  let fixture: ComponentFixture<SideBigmenuComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [SideBigmenuComponent]
    });
    fixture = TestBed.createComponent(SideBigmenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
