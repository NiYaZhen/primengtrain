import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PanelMenuModule } from 'primeng/panelmenu';

@Component({
  selector: 'app-side-bigmenu',
  standalone: true,
  imports: [CommonModule, PanelMenuModule],
  templateUrl: './side-bigmenu.component.html',
  styleUrls: ['./side-bigmenu.component.scss'],
})
export class SideBigmenuComponent {
  items!: any[];

  ngOnInit() {
    this.items = [
      {
        label: '診斷開立',
        icon: 'pi pi-file',
        items: [
          {
            label: '字首查詢',
          },
          {
            label: '專科診斷',
          },
          {
            label: '常用分類',
          },
        ],
      },
      {
        label: '開立藥囑',
        icon: 'pi pi-file',
        items: [
          {
            label: '院內藥品',
          },
          {
            label: '用藥Ditto',
          },
          {
            label: '自備藥物',
          },
          {
            label: '抗微生物藥品',
          },
        ],
      },
      {
        label: 'Title',
        icon: 'pi pi-file',
        items: [
          {
            label: '檢驗檢查項目',
          },
          {
            label: '放射檢查',
          },
          {
            label: '內科檢查',
          },
          {
            label: '外科檢查',
          },
          {
            label: '婦產檢查',
          },
          {
            label: '兒童檢查',
          },
          {
            label: '其他檢查',
          },
          {
            label: '治療及處置',
          },
          {
            label: '院內用藥',
          },
          {
            label: '自備用藥',
          },
        ],
      },
      {
        label: '醫囑處方紀錄查詢',
        icon: 'pi pi-fw pi-search',
        items: [
          {
            label: '藥囑紀錄',
          },
          {
            label: '用藥紀錄',
          },
          {
            label: '醫囑紀錄',
          },
        ],
      },
      {
        label: '檢驗檢查報告查詢',
        icon: 'pi pi-fw pi-search',
        items: [
          {
            label: '生命跡象',
          },
          {
            label: '檢驗報告',
          },
          {
            label: '檢查報告',
          },
        ],
      },
    ];
  }
}
