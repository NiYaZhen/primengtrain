import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardModule } from 'primeng/card';
import { ScrollerModule } from 'primeng/scroller';
import { TableModule } from 'primeng/table';

@Component({
  selector: 'app-left-content-card',
  standalone: true,
  imports: [CommonModule, CardModule, ScrollerModule, TableModule],
  templateUrl: './left-content-card.component.html',
  styleUrls: ['./left-content-card.component.scss'],
})
export class LeftContentCardComponent {
  @Input() card: any;
  items!: string[];
  tables!: string[];
  products: any;
  IsContent = true;

  ngOnInit() {
    this.items = Array.from({ length: 1 }).map(
      (_, i) => `${this.card.content}`,
    );
  }
}
