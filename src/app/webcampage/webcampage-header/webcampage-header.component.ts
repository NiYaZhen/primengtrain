import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-webcampage-header',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './webcampage-header.component.html',
  styleUrls: ['./webcampage-header.component.scss']
})
export class WebcampageHeaderComponent {

}
