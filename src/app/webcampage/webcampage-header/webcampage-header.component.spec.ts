import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WebcampageHeaderComponent } from './webcampage-header.component';

describe('WebcampageHeaderComponent', () => {
  let component: WebcampageHeaderComponent;
  let fixture: ComponentFixture<WebcampageHeaderComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [WebcampageHeaderComponent]
    });
    fixture = TestBed.createComponent(WebcampageHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
