import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SplitterModule } from 'primeng/splitter';
import {
  trigger,
  state,
  style,
  animate,
  transition,
  // ...
} from '@angular/animations';
import { SideMenuComponent } from '../side-menu/side-menu.component';
import { SideBigmenuComponent } from '../side-bigmenu/side-bigmenu.component';
import { WebcampageHeaderComponent } from '../webcampage-header/webcampage-header.component';
import { WebcampageContentComponent } from '../webcampage-content/webcampage-content.component';
import { WebcampageContentHeaderComponent } from '../webcampage-content-header/webcampage-content-header.component';

@Component({
  selector: 'app-webcampage-main',
  standalone: true,
  templateUrl: './webcampage-main.component.html',
  styleUrls: ['./webcampage-main.component.scss'],
  animations: [
    trigger('openClose', [
      // ...
      state(
        'open',
        style({
          height: '1080px',
          width: '240px',
          opacity: 1,
          backgroundColor: 'yellow',
        }),
      ),
      state(
        'closed',
        style({
          height: '1080px',
          width: '60px',
          padding: '50px',
          opacity: 0.8,
          backgroundColor: 'blue',
        }),
      ),
      transition('open => closed', [animate('1s')]),
      transition('closed => open', [animate('0.5s')]),
    ]),
    trigger('openCloseIcon', [
      // ...
      state(
        'open',
        style({
          height: '1080px',
          width: '240px',
          opacity: 1,
          backgroundColor: 'yellow',
        }),
      ),
      state(
        'closed',
        style({
          height: '1080px',
          width: '60px',
          opacity: 0.8,
          backgroundColor: 'blue',
        }),
      ),
      transition('open => closed', [animate('1s')]),
      transition('closed => open', [animate('0.5s')]),
    ]),
    trigger('openCloseIcon', [
      // ...
      state(
        'open',
        style({
          height: '1080px',
          width: '240px',
          opacity: 1,
          backgroundColor: 'yellow',
        }),
      ),
      state(
        'closed',
        style({
          height: '1080px',
          width: '60px',
          opacity: 0.8,
          backgroundColor: 'blue',
        }),
      ),
      transition('open => closed', [animate('1s')]),
      transition('closed => open', [animate('0.5s')]),
    ]),
    trigger('buttonAnimation', [
      state('expanded', style({ transform: 'rotate(180deg)' })),
      state('collapsed', style({ transform: 'rotate(0)' })),
      transition('expanded <=> collapsed', animate('300ms ease-out')),
    ]),
  ],
  imports: [
    CommonModule,
    SplitterModule,
    SideMenuComponent,
    SideBigmenuComponent,
    WebcampageHeaderComponent,
    WebcampageContentComponent,
    WebcampageContentHeaderComponent,
  ],
})
export class WebcampageMainComponent {
  isOpen = true;

  toggle() {
    this.isOpen = !this.isOpen;
  }

  isSideNavOpen = false;
  toggleIcon = 'pi pi-chevron-right';

  toggleSideNav() {
    this.isSideNavOpen = !this.isSideNavOpen;
    this.toggleIcon = this.isSideNavOpen
      ? 'pi pi-chevron-left'
      : 'pi pi-chevron-right';
  }
}
