import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WebcampageMainComponent } from './webcampage-main.component';

describe('WebcampageMainComponent', () => {
  let component: WebcampageMainComponent;
  let fixture: ComponentFixture<WebcampageMainComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [WebcampageMainComponent]
    });
    fixture = TestBed.createComponent(WebcampageMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
