import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WebcampageContentHeaderComponent } from './webcampage-content-header.component';

describe('WebcampageContentHeaderComponent', () => {
  let component: WebcampageContentHeaderComponent;
  let fixture: ComponentFixture<WebcampageContentHeaderComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [WebcampageContentHeaderComponent]
    });
    fixture = TestBed.createComponent(WebcampageContentHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
