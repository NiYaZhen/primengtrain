import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { ButtonModule } from 'primeng/button';

@Component({
  selector: 'app-webcampage-content-header',
  standalone: true,
  imports: [CommonModule, DropdownModule, FormsModule, ButtonModule],
  templateUrl: './webcampage-content-header.component.html',
  styleUrls: ['./webcampage-content-header.component.scss'],
})
export class WebcampageContentHeaderComponent {
  ditto: any[] | undefined;
  print: any[] | undefined;

  selectedDitto: any | undefined;
  selectedPrint: any | undefined;

  ngOnInit() {
    this.ditto = [
      { name: '醫生上次', code: 'NY' },
      { name: '科別上次', code: 'RM' },
      { name: '詳細選擇', code: 'LDN' },
      { name: '預設設定', code: 'IST' },
    ];
    this.print = [{ name: '列印', code: 'NY' }];
  }
}
