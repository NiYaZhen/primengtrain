import { TestBed } from '@angular/core/testing';

import { WebcapageService } from './webcapage.service';

describe('WebcapageService', () => {
  let service: WebcapageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(WebcapageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
