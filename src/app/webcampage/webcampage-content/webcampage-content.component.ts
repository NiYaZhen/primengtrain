import { Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WebcampageContentHeaderComponent } from '../webcampage-content-header/webcampage-content-header.component';
import { LeftContentCardComponent } from '../left-content-card/left-content-card.component';
import { card } from '../webcapage';
import { WebcapageService } from '../webcapage.service';

@Component({
  selector: 'app-webcampage-content',
  standalone: true,
  templateUrl: './webcampage-content.component.html',
  styleUrls: ['./webcampage-content.component.scss'],
  imports: [
    CommonModule,
    WebcampageContentHeaderComponent,
    LeftContentCardComponent,
  ],
})
export class WebcampageContentComponent {
  webcampageService: WebcapageService = inject(WebcapageService);
  cards: card[] = [];
  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    this.cards = this.webcampageService.getcards();
  }
}
