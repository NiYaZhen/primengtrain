import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WebcampageContentComponent } from './webcampage-content.component';

describe('WebcampageContentComponent', () => {
  let component: WebcampageContentComponent;
  let fixture: ComponentFixture<WebcampageContentComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [WebcampageContentComponent]
    });
    fixture = TestBed.createComponent(WebcampageContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
