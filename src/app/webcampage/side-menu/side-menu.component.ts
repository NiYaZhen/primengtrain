import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SplitterModule } from 'primeng/splitter';
import {
  trigger,
  state,
  style,
  animate,
  transition,
  // ...
} from '@angular/animations';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'app-side-menu',
  standalone: true,
  imports: [CommonModule, SplitterModule, RouterLink],
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss'],
  animations: [
    trigger('openClose', [
      // ...
      state(
        'open',
        style({
          height: '100vh',
          width: '240px',
          opacity: 1,
          backgroundColor: '#D8DBD7',
        }),
      ),
      state(
        'closed',
        style({
          height: '100vh',
          width: '60px',
          opacity: 1,
          backgroundColor: '#D8DBD7',
        }),
      ),
      transition('open => closed', [animate('0.5s')]),
      transition('closed => open', [animate('0.5s')]),
    ]),
  ],
})
export class SideMenuComponent {
  isOpen = false;

  toggle() {
    this.isOpen = !this.isOpen;
  }
}
