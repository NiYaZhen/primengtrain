import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PasswordComponent } from '../password/password.component';
import { FormsModule } from '@angular/forms';
import { AccountComponent } from '../account/account.component';
import { ButtonModule } from 'primeng/button';
import { RouterOutlet, RouterLink } from '@angular/router';

@Component({
  selector: 'app-login',
  standalone: true,
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  imports: [
    CommonModule,
    PasswordComponent,
    FormsModule,
    AccountComponent,
    ButtonModule,
    RouterOutlet,
    RouterLink,
  ],
})
export class LoginComponent {}
