import { Routes } from '@angular/router';

export const routes: Routes = [
  {
    path: '',
    loadComponent: () =>
      import('./login/login/login.component').then((m) => m.LoginComponent),
  },
  {
    path: 'homepage',
    loadComponent: () =>
      import('./homepage/homepage-main/homepage-main.component').then(
        (m) => m.HomepageMainComponent,
      ),
  },
  {
    path: 'webcampage',
    loadComponent: () =>
      import('./webcampage/webcampage-main/webcampage-main.component').then(
        (m) => m.WebcampageMainComponent,
      ),
  },
  {
    path: 'login',
    loadComponent: () =>
      import('./login/login/login.component').then((m) => m.LoginComponent),
  },
];
