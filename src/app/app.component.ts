import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterOutlet } from '@angular/router';
import { CalendarModule } from 'primeng/calendar';
import {
  FormsModule,
  ReactiveFormsModule,
  FormControl,
  FormGroup,
} from '@angular/forms';
import { LoginComponent } from './login/login/login.component';
import { HomepageMainComponent } from './homepage/homepage-main/homepage-main.component';
import { WebcampageMainComponent } from './webcampage/webcampage-main/webcampage-main.component';

@Component({
  selector: 'app-root',
  standalone: true,
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  imports: [
    CommonModule,
    RouterOutlet,
    FormsModule,
    CalendarModule,
    ReactiveFormsModule,
    LoginComponent,
    HomepageMainComponent,
    WebcampageMainComponent,
  ],
})
export class AppComponent {
  formGroup!: FormGroup;
  color!: string | undefined;
  ngOnInit() {
    this.formGroup = new FormGroup({
      date: new FormControl<Date | null>(null),
    });
  }
}
