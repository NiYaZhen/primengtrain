import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AsideMenuComponent } from '../aside-menu/aside-menu.component';
import { NewsComponent } from '../news/news.component';
import { HomepageHeaderComponent } from '../homepage-header/homepage-header.component';
import { FavoriteComponent } from '../favorite/favorite.component';

@Component({
  selector: 'app-homepage-main',
  standalone: true,
  templateUrl: './homepage-main.component.html',
  styleUrls: ['./homepage-main.component.scss'],
  imports: [
    CommonModule,
    AsideMenuComponent,
    NewsComponent,
    HomepageHeaderComponent,
    FavoriteComponent,
  ],
})
export class HomepageMainComponent {}
