import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-homepage-header',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './homepage-header.component.html',
  styleUrls: ['./homepage-header.component.scss']
})
export class HomepageHeaderComponent {

}
