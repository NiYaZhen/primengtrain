import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardModule } from 'primeng/card';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';

@Component({
  selector: 'app-news',
  standalone: true,
  imports: [CommonModule, CardModule, TableModule, ButtonModule],
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss'],
})
export class NewsComponent {
  // products!: Product[];

  // constructor(private productService: ProductService) {}

  ngOnInit() {
    // this.productService.getProductsMini().then((data) => {
    //   this.products = data;
    // });
  }

  products = [
    {
      date: '2023-07-05 11:11',
      content: '您目前有22張簽核表單需簽核。',
      path: '請點選進入',
    },
    {
      date: '2023-07-03 09:32',
      content: '您目前有14張簽核表單需簽核。',
      path: '請點選進入',
    },
    {
      date: '2023-06-30 16:29',
      content: '您目前有6張簽核表單需簽核。',
      path: '請點選進入',
    },
    {
      date: '2023-06-25 13:47',
      content: '您有衛材申請待審核。',
      path: '',
    },
    {
      date: '2023-06-25 13:47',
      content: '您有衛材申請待審核。',
      path: '',
    },
    {
      date: '2023-06-25 13:47',
      content: '您有衛材申請待審核。',
      path: '',
    },
    {
      date: '2023-06-25 13:47',
      content: '您有衛材申請待審核。',
      path: '',
    },
    {
      date: '2023-06-25 13:47',
      content: '您有衛材申請待審核。',
      path: '',
    },
    {
      date: '2023-06-25 13:47',
      content: '您有衛材申請待審核。',
      path: '',
    },
    {
      date: '2023-06-25 13:47',
      content: '您有衛材申請待審核。',
      path: '',
    },

    {
      date: '2023-06-25 13:47',
      content: '您有衛材申請待審核。',
      path: '',
    },
    {
      date: '2023-06-25 13:47',
      content: '您有衛材申請待審核。',
      path: '',
    },
    {
      date: '2023-06-25 13:47',
      content: '您有衛材申請待審核。',
      path: '',
    },

    {
      date: '2023-06-25 13:47',
      content: '您有衛材申請待審核。',
      path: '',
    },
    {
      date: '2023-06-25 13:47',
      content: '您有衛材申請待審核。',
      path: '',
    },
    {
      date: '2023-06-25 13:47',
      content: '您有衛材申請待審核。',
      path: '',
    },
    {
      date: '2023-06-25 13:47',
      content: '您有衛材申請待審核。',
      path: '',
    },
    {
      date: '2023-06-25 13:47',
      content: '您有衛材申請待審核。',
      path: '',
    },
    {
      date: '2023-06-25 13:47',
      content: '您有衛材申請待審核。',
      path: '',
    },
    {
      date: '2023-06-25 13:47',
      content: '您有衛材申請待審核。',
      path: '',
    },
    {
      date: '2023-06-25 13:47',
      content: '您有衛材申請待審核。',
      path: '',
    },
    {
      date: '2023-06-25 13:47',
      content: '您有衛材申請待審核。',
      path: '',
    },
  ];
}
