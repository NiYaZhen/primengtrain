import { FavoriteCard } from './../homepage';
import { Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonModule } from 'primeng/button';
import { FavoriteCardComponent } from '../favorite-card/favorite-card.component';
import { HomepageService } from '../homepage.service';

@Component({
  selector: 'app-favorite',
  standalone: true,
  templateUrl: './favorite.component.html',
  styleUrls: ['./favorite.component.scss'],
  imports: [CommonModule, ButtonModule, FavoriteCardComponent],
})
export class FavoriteComponent {
  cards: FavoriteCard[] = [];
  homepageService: HomepageService = inject(HomepageService);

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    this.cards = this.homepageService.getFavoriteCards();
  }
}
