import { Component, Input, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardModule } from 'primeng/card';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { RouterLink } from '@angular/router';
import { FavoriteCard } from '../homepage';

@Component({
  selector: 'app-favorite-card',
  standalone: true,
  imports: [CommonModule, CardModule, RouterLink],
  templateUrl: './favorite-card.component.html',
  styleUrls: ['./favorite-card.component.scss'],
})
export class FavoriteCardComponent implements OnInit {
  @Input() card!: FavoriteCard;
  trustedSvg!: SafeHtml;

  constructor(private sanitizer: DomSanitizer) {}

  ngOnInit(): void {
    this.trustedSvg = this.sanitizer.bypassSecurityTrustHtml(this.card.icon);
  }
}
