export interface FavoriteCard {
  icon: string;
  system: string;
  version: string;
  color: string;
  path: string;
}
